<?php
return [
    'Вхід в систему управління' => 'Вход в систему управления',
    'Логін' => 'Логин',
    'Пароль' => 'Пароль',
    'Ввійти' => 'Войти',
    'Забули пароль?' => 'Забыли пароль?',
    'Запам\'ятати мене' => 'Запомнить меня',
    'Немає аккаунта?' => 'Нет аккаунта?',
    'Реєстрація' => 'Регистрация',
    'Ваша система управління' => 'Ваша система управления',
    'Розроблена, щоб покращити вам життя...' => 'Розроблена, щоб покращити вам життя...',
    'Поле не може бути пустим' => 'Поле не може бути пустим',
    'Цей email уже використовується' => 'Цей email уже використовується',
    'Цей логін уже використовується' => 'Цей логін уже використовується',
    'Введіть ваші дані' => 'Введіть ваші дані',
    'Ваше ім\'я' => 'Ваше ім\'я',
    'Паролі не співпадають' => 'Паролі не співпадають',
    'Повторіть пароль' => 'Повторіть пароль',
    'Зареєстуватися' => 'Зареєстуватися',
    'Відміна' => 'Відміна',
    'Дякуємо за реєстрацію! Ви зможете використовувати систему після одоброення вашого акаунта адміністратором' => 'Дякуємо за реєстрацію! Ви зможете використовувати систему після одоброення вашого акаунта адміністратором',
    'Пошук' => 'Пошук',
    'сповіщення' => 'сповіщення',
    'сповіщень' => 'сповіщень',
    'Статистика' => 'Статистика',
    'Повідомлення з сайту' => 'Повідомлення з сайту',
    'Додати подію' => 'Додати подію',
    'Календар' => 'Календар',
    'Елементи' => 'Елементи',
    'Про нас' => 'Про нас',
    'Додати' => 'Додати',
    'Завантаження' => 'Завантаження',
    'Нічого не знайдено' => 'Нічого не знайдено',
    'Перша' => 'Перша',
    'Попередня' => 'Попередня',
    'Наступна' => 'Наступна',
    'Більше' => 'Більше',
    'К-сть сторінок' => 'К-сть сторінок',
    'Показувати по' => 'Показувати по',
    'Показано' => 'Показано',
    'із' => 'із',
    'елементів' => 'елементів',
    'Остання' => 'Остання',
    'Всі' => 'Всі',
    'Пошук...' => 'Пошук...',
    'Додати новий елемент' => 'Додати новий елемент',
    'Зберегти' => 'Зберегти',
    'Зачекайте...' => 'Зачекайте...',
    'Редагувати елемент' => 'Редагувати елемент',
    'Підтримка' => 'Підтримка',
    'Логування' => 'Логування',
    'Додано новий елемент в блок' => 'Додано новий елемент в блок',
    'Відредаговано елемент з блоку' => 'Відредаговано елемент з блоку',
    'Видалено елемент з блоку' => 'Видалено елемент з блоку',
    'Профіль' => 'Профіль',
    'Допомога' => 'Допомога',
    'Вийти' => 'Вийти',
    'Доступні формати:' => 'Доступні формати:',
    'Перетягніть файл або виберіть з комп\'ютера' => 'Перетягніть файл або виберіть з комп\'ютера',
    'Вибрати файл' => 'Вибрати файл',
    'Імпорт' => 'Імпорт',
    'Вибрані' => 'Вибрані',
    'Експорт' => 'Експорт',
    'Дії' => 'Дії',
    'виберіть' => 'виберіть',
    'Ви допустили помилки!' => 'Ви допустили помилки!',
    'Не допустимий формат файлу або великий розмір файлу' => 'Не допустимий формат файлу або великий розмір файлу',
    'Налаштування' => 'Налаштування'
];