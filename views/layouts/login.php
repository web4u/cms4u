<?php

/* @var $this \yii\web\View */
/* @var $content string */

use backend\assets\AppAsset;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use common\widgets\Alert;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>" >
<!-- begin::Head -->
<head>
    <meta charset="<?= Yii::$app->charset ?>" />
    <title>
        <?= Html::encode($this->title) ?>
    </title>
    <?= Html::csrfMetaTags() ?>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!--begin::Web font -->
    <script src="https://ajax.googleapis.com/ajax/libs/webfont/1.6.16/webfont.js"></script>
    <script src="/admin/assets/vendors/base/vendors.bundle.js" type="text/javascript"></script>
    <script>
        WebFont.load({
            google: {"families":["Open Sans:300,400,500,600,700","Roboto:300,400,500,600,700"]},
            active: function() {
                sessionStorage.fonts = true;
            }
        });
    </script>
    <!--end::Web font -->
    <link rel="shortcut icon" href="/admin/assets/demo/default/media/img/logo/favicon.ico" />
    <?php $this->head() ?>
</head>
<!-- end::Head -->
<!-- end::Body -->
<body class="m--skin- m-header--fixed m-header--fixed-mobile m-aside-left--enabled m-aside-left--skin-dark m-aside-left--offcanvas m-footer--push m-aside--offcanvas-default"  >
<?php $this->beginBody() ?>
<!-- begin:: Page -->
<div class="m-grid m-grid--hor m-grid--root m-page">
    <?=$content?>
</div>
<!-- end:: Page -->
<?php $this->endBody() ?>
</body>
<!-- end::Body -->
</html>
<?php $this->endPage() ?>
