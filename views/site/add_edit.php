<?php
use dosamigos\fileupload\FileUploadUI;
$this->title=$pageName;
?>
<div class="m-grid__item m-grid__item--fluid m-wrapper">
    <div class="m-content">
        <?php
        if (isset($error)){
        ?>
        <div class="m-alert m-alert--icon alert alert-danger" role="alert">
            <div class="m-alert__icon">
                <i class="flaticon-danger"></i>
            </div>
            <div class="m-alert__text">
                <strong><?php echo Yii::t('app', 'Ви допустили помилки!') ?></strong>
                <?
                foreach ($error as $f){
                    for ($e=0;$e<count($f);$e++){
                        ?>
                        <div><?php echo $f[$e]?></div>
                        <?php
                    }
                }
                ?>
            </div>
        </div>
        <?php } ?>
        <div class="row">
            <div class="col-lg-12">
                <!--begin::Portlet-->
                <div class="m-portlet">
                    <div class="m-portlet__head">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title">
						<span class="m-portlet__head-icon m--hide">
						<i class="la la-gear"></i>
						</span>
                                <h3 class="m-portlet__head-text">
                                    <?php echo $this->title?>
                                </h3>
                            </div>
                        </div>
                    </div>
                    <!--begin::Form-->
                    <?php $form = \yii\bootstrap\ActiveForm::begin(['action'=>'/admin/'.$table.'/save','enableAjaxValidation' => true,'options'=>['class'=>'m-form','data-pjax' => true,'fieldConfig' => ['options' => ['tag' => false]]]]); ?>
                    <?php
                    if (isset($id)){
                        ?>
                        <input type="hidden" name="id" value="<?php echo $id ?>">
                        <?php
                    }
                    ?>
                        <div class="m-portlet__body">
                            <div class="m-form__section m-form__section--first">
                                <?php
                                $langs = \frontend\models\Lang::find()->where(['default'=>0])->all();
                                foreach ($model->rows() as $element) {
                                    switch ($element['view']['type']){
                                        case 'input':
                                            ?>
                                            <div class="form-group m-form__group row">
                                                <label class="col-lg-3 col-form-label"><?php echo $model::attributeLabels()[$element['table']['field']]?>:</label>
                                                <div class="col-lg-6">
                                                    <?php
                                                    $attr=isset($element['view']['attr'])?$element['view']['attr']:'';
                                                    echo $form->field($model, $element['table']['field'])->textInput(['class'=>'form-control m-input',$attr])->label(false);
                                                    ?>
                                                </div>
                                            </div>
                                            <?php
                                            if (isset($element['view']['lang'])){
                                                foreach ($langs as $lang){
                                                    if (isset($id)) {
                                                        $existValue = \common\models\Translits::find()->where(['table' => $table, 'row' => $element['table']['field'], 'element' => $id, 'lang' => $lang['id']])->one()['text'];
                                                    } else {
                                                        $existValue = '';
                                                    }
                                                    ?>
                                                    <div class="form-group m-form__group row">
                                                        <label class="col-lg-3 col-form-label"><?php echo $model::attributeLabels()[$element['table']['field']]?> [<?=$lang['name']?>]:</label>
                                                        <div class="col-lg-6">
                                                            <div class="form-group">

                                                                <input type="text" class="form-control m-input" name="Translits[<?=$element['table']['field']?>][<?=$lang['id']?>]" value="<?=$existValue?>">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <?php
                                                }
                                            }
                                            break;

                                        case 'password':
                                            ?>
                                            <div class="form-group m-form__group row">
                                                <label class="col-lg-3 col-form-label"><?php echo $model::attributeLabels()[$element['table']['field']]?>:</label>
                                                <div class="col-lg-6">
                                                    <?php
                                                    $attr=isset($element['view']['attr'])?$element['view']['attr']:'';
                                                    $model[$element['table']['field']]='';
                                                    echo $form->field($model, $element['table']['field'])->textInput(['class'=>'form-control m-input',$attr])->label(false);
                                                    ?>
                                                </div>
                                            </div>
                                        <?php
                                        break;

                                        case 'datepicker':
                                            ?>
                                            <div class="form-group m-form__group row">
                                                <label class="col-lg-3 col-form-label"><?php echo $model::attributeLabels()[$element['table']['field']]?>:</label>
                                                <div class="col-lg-6">
                                                    <?php
                                                    $attr=isset($element['view']['attr'])?$element['view']['attr']:'';
                                                    echo $form->field($model, $element['table']['field'])->textInput(['class'=>'form-control m-input m_datepicker',$attr])->label(false);
                                                    ?>
                                                </div>
                                            </div>
                                            <script>
                                                $(document).on('pjax:success', function(e) {
                                                    $(".m_datepicker").datepicker({
                                                        todayHighlight: !0,
                                                        orientation: "bottom left",
                                                        templates: {
                                                            leftArrow: '<i class="la la-angle-left"></i>',
                                                            rightArrow: '<i class="la la-angle-right"></i>'
                                                        },
                                                        language: 'ru',
                                                        format: {
                                                            /*
                                                             * Say our UI should display a week ahead,
                                                             * but textbox should store the actual date.
                                                             * This is useful if we need UI to select local dates,
                                                             * but store in UTC
                                                             */
                                                            toDisplay: function (date, format, language) {
                                                                var d = new Date(date);
                                                                var day = ((d.getDate()).toString()).length==1?'0'+d.getDate():d.getDate();
                                                                var month = ((d.getMonth()+1).toString()).length==1?'0'+(d.getMonth()+1):(d.getMonth()+1);
                                                                return d.getFullYear()+'-'+month+'-'+day;
                                                            },
                                                            toValue: function (date, format, language) {
                                                                var d = new Date(date);
                                                                var day = ((d.getDate()).toString()).length==1?'0'+d.getDate():d.getDate();
                                                                var month = ((d.getMonth()+1).toString()).length==1?'0'+(d.getMonth()+1):(d.getMonth()+1);
                                                                return d.getFullYear()+'-'+month+'-'+day;
                                                            }
                                                        }
                                                    })
                                                });
                                            </script>
                                            <?php
                                            break;

                                        case 'textarea':
                                            ?>
                                            <div class="form-group m-form__group row">
                                                <label class="col-lg-3 col-form-label"><?php echo $model::attributeLabels()[$element['table']['field']]?>:</label>
                                                <div class="col-lg-6">
                                                    <?php
                                                    $attr=isset($element['view']['attr'])?$element['view']['attr']:'';
                                                    echo $form->field($model, $element['table']['field'])->textarea(['class'=>'form-control m-input','rows'=>5,$attr])->label(false);
                                                    ?>
                                                </div>
                                            </div>
                                            <?php
                                            if (isset($element['view']['lang'])){
                                                foreach ($langs as $lang){
                                                    if (isset($id)) {
                                                        $existValue = \common\models\Translits::find()->where(['table' => $table, 'row' => $element['table']['field'], 'element' => $id, 'lang' => $lang['id']])->one()['text'];
                                                    } else {
                                                        $existValue = '';
                                                    }
                                                    ?>
                                                    <div class="form-group m-form__group row">
                                                        <label class="col-lg-3 col-form-label"><?php echo $model::attributeLabels()[$element['table']['field']]?> [<?=$lang['name']?>]:</label>
                                                        <div class="col-lg-6">
                                                            <div class="form-group">

                                                                <textarea class="form-control m-input" name="Translits[<?=$element['table']['field']?>][<?=$lang['id']?>]" rows="5"><?=$existValue?></textarea>
                                                            </div>
                                                        </div>
                                                    </div>
                                                <?php
                                                }
                                            }
                                            break;

                                        case 'select':
                                            ?>
                                            <div class="form-group m-form__group row">
                                                <label class="col-lg-3 col-form-label"><?php echo $model::attributeLabels()[$element['table']['field']]?>:</label>
                                                <div class="col-lg-6">
                                                    <select class="form-control select-custom" name="<?php echo $class?>[<?php echo $element['table']['field']?>]">
                                                        <option value="">
                                                            <?php echo Yii::t('app', 'виберіть'); ?>
                                                        </option>
                                                        <?php
                                                        $select=Yii::$app->db->createCommand("select * from ".$element['view']['table']['name']." order by id")->queryAll();
                                                        foreach ($select as $option){
                                                            ?>
                                                            <option <?php if ($option[$element['view']['table']['value']]==$model[$element['table']['field']]) echo 'selected'; ?> value="<?=$option[$element['view']['table']['value']]?>">
                                                                <?php
                                                                foreach ($element['view']['table']['text'] as $option_text){
                                                                    echo $option[$option_text].' ';
                                                                }
                                                                ?>
                                                            </option>
                                                        <?php } ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <script>
                                                $(document).on('pjax:success', function(e) {
                                                    $(".select-custom").select2();
                                                });
                                            </script>
                                            <?php
                                            break;

                                        case 'checkbox':
                                            ?>
                                            <div class="form-group m-form__group row">
                                                <label class="col-lg-3 col-form-label"><?php echo $model::attributeLabels()[$element['table']['field']]?>:</label>
                                                <div class="col-lg-6">
                                                    <div class="m-checkbox-list">
                                                        <?php
                                                        if (isset($id)) {
                                                            $ex_values = json_decode($model[$element['table']['field']]);
                                                            if (empty($ex_values)){
                                                                $ex_values=[];
                                                            }
                                                        } else {
                                                            $ex_values = [];
                                                        }
                                                        $select=Yii::$app->db->createCommand("select * from ".$element['view']['table']['name']." order by id")->queryAll();
                                                        foreach ($select as $option){
                                                            ?>
                                                            <label class="m-checkbox">
                                                                <input type="checkbox" <?php if (in_array($option[$element['view']['table']['value']], $ex_values)) echo 'checked'; ?> value="<?=$option[$element['view']['table']['value']]?>" name="<?php echo $class?>[<?php echo $element['table']['field']?>][]">
                                                                <?php
                                                                foreach ($element['view']['table']['text'] as $option_text){
                                                                    echo $option[$option_text].' ';
                                                                }
                                                                ?>
                                                                <span></span>
                                                            </label>
                                                        <?php } ?>
                                                    </div>
                                                </div>
                                            </div>
                                        <?php
                                        break;

                                        case 'radio':
                                            ?>
                                            <div class="m-form__group form-group row">
                                                <label class="col-lg-3 col-form-label"><?php echo $model::attributeLabels()[$element['table']['field']]?>:</label>
                                                <div class="col-lg-6 m-radio-list">
                                                    <?php
                                                    foreach ($element['view']['data'] as $k=>$v){
                                                        $checked=$model[$element['table']['field']]==$k?'checked':'';
                                                    ?>
                                                    <label class="m-radio">
                                                        <input type="radio" name="<?php echo $class?>[<?php echo $element['table']['field']?>]" <?php echo $checked ?> value="<?php echo $k ?>"> <?php echo $v ?>
                                                        <span></span>
                                                    </label>
                                                    <?php } ?>
                                                </div>
                                            </div>
                                            <?php
                                            break;

                                        case 'file':
                                            ?>
                                            <div class="form-group m-form__group row">
                                                <label class="col-lg-3 col-form-label"><?php echo $model::attributeLabels()[$element['table']['field']]?>:</label>
                                                <div class="col-lg-9">
                                                    <div class="m-alert m-alert--icon m-alert--outline alert alert-info alert-dismissible fade show" role="alert">
                                                        <div class="m-alert__icon">
                                                            <i class="la la-file-o"></i>
                                                        </div>
                                                        <div class="m-alert__text">
                                                            <strong><?php echo Yii::t('app', 'Доступні формати:') ?></strong> <?php echo $element['view']['attr']['format']?>
                                                        </div>
                                                        <button type="button" class="btn button-load btn-info" onclick="$(this).next().click();" style="margin-top: 20px;"><?php echo Yii::t('app', 'Вибрати файл') ?></button>
                                                        <input type="file" data-maxsize="<?php echo $element['view']['attr']['maxsize']?>" data-extend="<?php echo $element['view']['attr']['format']?>" class="hide" id="load-file-<?php echo $element['table']['field']?>" <?php if ($element['view']['attr']['multiple']) echo 'multiple'; ?>>
                                                        <script>
                                                            $("#load-file-<?php echo $element['table']['field']?>").jLoad({
                                                                beforeload: function () {
                                                                    $("#load-file-<?php echo $element['table']['field']?>").prev().addClass('m-loader m-loader--light m-loader--right');
                                                                },
                                                                onsuccess: function(res){
                                                                    <?php if ($element['view']['attr']['multiple']){ ?>
                                                                    $("#box-to-files-<?php echo $element['table']['field']?>").append('<div class="box-to-load-image"><i class="la la-trash"></i><img src="'+res+'" alt="" class="img-thumbnail"><input type="hidden" value="'+res+'" name="<?php echo $class?>[<?php echo $element['table']['field']?>][]"></div>');
                                                                    <?php } else { ?>
                                                                    $("#box-to-files-<?php echo $element['table']['field']?>").html('<div class="box-to-load-image"><i class="la la-trash"></i><img src="'+res+'" alt="" class="img-thumbnail"><input type="hidden" value="'+res+'" name="<?php echo $class?>[<?php echo $element['table']['field']?>][]"></div>');
                                                                    <?php } ?>
                                                                    $("#load-file-<?php echo $element['table']['field']?>").prev().removeClass('m-loader m-loader--light m-loader--right');
                                                                },
                                                                onerror: function(){
                                                                    alert("<?php echo Yii::t('app', 'Не допустимий формат файлу або великий розмір файлу')?>")
                                                                }
                                                            });
                                                            $(".box-to-load-image").sortable();
                                                            $(".box-to-load-image").disableSelection();
                                                        </script>
                                                    </div>
                                                    <div id="box-to-files-<?php echo $element['table']['field']?>" class="clearfix alert-image" data-toggle="all-image">
                                                        <?php
                                                        if (isset($model[$element['table']['field']])){
                                                            $pics=json_decode($model[$element['table']['field']]);
                                                            foreach ($pics as $pic){
                                                                ?>
                                                                <div class="box-to-load-image">
                                                                    <i class="la la-trash"></i>
                                                                    <img src="<?php echo $pic?>" alt="" class="img-thumbnail">
                                                                    <input type="hidden" value="<?php echo $pic ?>" name="<?php echo $class?>[<?php echo $element['table']['field']?>][]">
                                                                </div>
                                                                <?php
                                                            }
                                                        }
                                                        ?>
                                                        <script>
                                                            $(function(){
                                                                $(".box-to-load-image").sortable();
                                                                $(".box-to-load-image").disableSelection();
                                                            });
                                                        </script>
                                                    </div>
                                                </div>
                                            </div>
                                            <?php
                                            break;

                                        case 'editor':
                                            ?>
                                            <div class="form-group m-form__group row">
                                                <label class="col-lg-3 col-form-label"><?php echo $model::attributeLabels()[$element['table']['field']]?>:</label>
                                                <div class="col-lg-9">
                                                    <?php
                                                    $attr=isset($element['view']['attr'])?$element['view']['attr']:'';
                                                    echo $form->field($model, $element['table']['field'])->textarea(['class'=>'form-control m-input','id'=>'editor-'.$element['table']['field'],'rows'=>5,$attr])->label(false);
                                                    ?>
                                                </div>
                                            </div>
                                            <script>
                                                $(document).on('pjax:success', function(e) {
                                                    CKEDITOR.replace( 'editor-<?php echo $element['table']['field']?>', {
                                                        language: 'ru',
                                                        filebrowserBrowseUrl : 'filemanager/dialog.php?type=2&editor=ckeditor&fldr=',
                                                        filebrowserUploadUrl : 'filemanager/dialog.php?type=2&editor=ckeditor&fldr=',
                                                        filebrowserImageBrowseUrl : 'filemanager/dialog.php?type=1&editor=ckeditor&fldr='
                                                    });
                                                });
                                            </script>
                                            <?php
                                            if (isset($element['view']['lang'])){
                                                foreach ($langs as $lang){
                                                    if (isset($id)) {
                                                        $existValue = \common\models\Translits::find()->where(['table' => $table, 'row' => $element['table']['field'], 'element' => $id, 'lang' => $lang['id']])->one()['text'];
                                                    } else {
                                                        $existValue = '';
                                                    }
                                                    ?>
                                                    <div class="form-group m-form__group row">
                                                        <label class="col-lg-3 col-form-label"><?php echo $model::attributeLabels()[$element['table']['field']]?> [<?=$lang['name']?>]:</label>
                                                        <div class="col-lg-9">
                                                            <div class="form-group">

                                                                <textarea class="form-control m-input" name="Translits[<?=$element['table']['field']?>][<?=$lang['id']?>]" id="translit-<?=$lang['url']?>"><?=$existValue?></textarea>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <script>
                                                        $(document).on('pjax:success', function(e) {
                                                            CKEDITOR.replace( 'translit-<?=$lang['url']?>', {
                                                                language: 'ru',
                                                                filebrowserBrowseUrl : 'filemanager/dialog.php?type=2&editor=ckeditor&fldr=',
                                                                filebrowserUploadUrl : 'filemanager/dialog.php?type=2&editor=ckeditor&fldr=',
                                                                filebrowserImageBrowseUrl : 'filemanager/dialog.php?type=1&editor=ckeditor&fldr='
                                                            });
                                                        });
                                                    </script>
                                                    <?php
                                                }
                                            }
                                            break;
                                    }
                                }
                                ?>
                            </div>
                            <?php
                            if (isset($model::$modules)){
                                foreach ($model::$modules as $file){
                                    require 'modules/'.$file.'.php';
                                }
                            }
                            ?>
                        </div>
                        <?php
//                        if ($table=='orders') {
//                            require $_SERVER['DOCUMENT_ROOT'] . '/backend/views/static/list_cart.php';
//                        }
                        ?>
                        <div class="m-portlet__foot m-portlet__foot--fit">
                            <div class="m-form__actions m-form__actions">
                                <div class="row">
                                    <div class="col-lg-3"></div>
                                    <div class="col-lg-6">
                                        <button type="submit" data-toggle="load-page" data-word="<?php echo Yii::t('app', 'Зачекайте...') ?>" class="btn btn-success"><?php echo Yii::t('app', 'Зберегти') ?></button>
                                        <a href="<?php echo \yii\helpers\Url::to('/admin/'.$table)?>" class="btn btn-secondary" data-toggle="load-page" data-word="<?php echo Yii::t('app', 'Зачекайте...') ?>"><?php echo Yii::t('app', 'Відміна') ?></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php
                    \yii\bootstrap\ActiveForm::end();
                    ?>
                    <!--end::Form-->
                </div>
                <!--end::Portlet-->
            </div>
        </div>
    </div>
</div>