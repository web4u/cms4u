<?php
$this->title = 'Web4U CMS';
$load=Yii::t('app', 'Завантаження');
$not_search=Yii::t('app', 'Нічого не знайдено');
$first=Yii::t('app', 'Перша');
$prev=Yii::t('app', 'Попередня');
$next=Yii::t('app', 'Наступна');
$last=Yii::t('app', 'Остання');
$more=Yii::t('app', 'Більше');
$page_number=Yii::t('app', 'К-сть сторінок');
$page_size=Yii::t('app', 'Показувати по');
$show=Yii::t('app', 'Показано');
$in=Yii::t('app', 'із');
$page=Yii::t('app', 'елементів');
$word_action=Yii::t('app', 'Дії');
$columns=str_replace(['[',']'], '', $columns);

/*** -= Якщо потрібні додаткові кнопки - визначаємо їх тут =- ***/
$more_action='<a data-toggle="load-page" href="/admin/'.$table.'/copy/\'+row.id+\'" class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill btn-outline-metal" title="'.Yii::t('app', 'Елементи').'"><i class="la la-copy"></i></a>';

$more_action.='<a data-toggle="load-page" href="/admin/'.$table.'/up/\'+row.id+\'" class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill btn-outline-metal" title="'.Yii::t('app', 'Елементи').'"><i class="la la-arrow-up"></i></a>';

$more_action.='<a data-toggle="load-page" href="/admin/'.$table.'/down/\'+row.id+\'" class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill btn-outline-metal" title="'.Yii::t('app', 'Елементи').'"><i class="la la-arrow-down"></i></a>';
if ($table=='pages') {
    $more_action.= '<a data-toggle="load-page" href="/admin/'.$table.'/elements/\'+row.id+\'" class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only  m-btn--pill btn-outline-metal" title="'.Yii::t('app', 'Елементи').'"><i class="la la-tasks"></i></a>';
}

//if ($table=='orders'){
//    $more_action = '<a data-toggle="load-page" href="/admin/'.$table.'/list_cart/\'+row.id+\'" class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="'.Yii::t('app', 'Список позиций заказа').'"><i class="la la-cart-arrow-down"></i></a>';
//}

if (isset($where)){
    $tableAjax = $table.'&'.$where;
} else {
    $tableAjax = $table;
}

$script = <<< JS
    var DatatableRemoteAjaxDemo = function () {
    //== Private functions

    // basic demo
    var demo = function () {

        var datatable = $('.m_datatable').mDatatable({
            // datasource definition
            data: {
                type: 'remote',
                source: {
                    read: {
                        url: '/admin/site/show-all?table=$tableAjax'
                    }
                },
                pageSize: 10,
                saveState: {
                    cookie: false,
                    webstorage: false
                },
                serverPaging: true,
                serverFiltering: true,
                serverSorting: true
            },

            // layout definition
            layout: {
                theme: 'default', // datatable theme
                class: '', // custom wrapper class
                scroll: false, // enable/disable datatable scroll both horizontal and vertical when needed.
                footer: false // display/hide footer
            },

            // column sorting
            sortable: true,

            pagination: true,

            columns: [$columns,{
				field: "actions",
				width: 200,
				title: '$word_action',
				sortable: false,
				locked:{'right':'xl'},
				overflow: 'visible',
				template: function (row) {
					return '<a href="/admin/$table/edit/'+row.id+'" class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill btn-outline-metal" title="Edit details"><i class="la la-edit"></i></a><a href="/admin/$table/delete/'+row.id+'" class="m-portlet__nav-link btn m-btn m-btn--hover-danger m-btn--icon m-btn--icon-only m-btn--pill  btn-outline-metal" title="Delete"><i class="la la-trash"></i></a>$more_action';
				}
			}],
            
            translate: { 
				records: {
					processing: '$load...',
					noRecords: '$not_search'
				},
				toolbar: {
					pagination: {
						items: {
							default: {
								first: '$first',
								prev: '$prev',
								next: '$next',
								last: '$last',
								more: '$more',
								input: '$page_number',
								select: '$page_size'
							},
							info: '$show {{start}} - {{end}} $in {{total}} $page'
						}
					}
				}
			}
        });

        var query = datatable.getDataSourceQuery();

        $('#m_form_search').on('keyup', function (e) {
            // shortcode to datatable.getDataSourceParam('query');
            var query = datatable.getDataSourceQuery();
            query.generalSearch = $(this).val().toLowerCase();
            query.table='$table';
            // shortcode to datatable.setDataSourceParam('query', query);
            datatable.setDataSourceQuery(query);
            datatable.load();
        }).val(query.generalSearch);

        $('[data-toggle=filter-table]').on('change', function () {
            // shortcode to datatable.getDataSourceParam('query');
            var query = datatable.getDataSourceQuery();
            query[$(this).attr('data-row')] = $(this).val().toLowerCase();
            query.table='$table';
            // shortcode to datatable.setDataSourceParam('query', query);
            datatable.setDataSourceQuery(query);
            datatable.load();
        }).val(typeof query[$(this).attr('data-row')] !== 'undefined' ? query[$(this).attr('data-row')] : '');

        $('[data-toggle=filter-table]').selectpicker();

    };

    return {
            // public functions
            init: function () {
                demo();
            }
        };
    }();

    jQuery(document).ready(function () {
        DatatableRemoteAjaxDemo.init();
        
        $(document).on('pjax:success', function(e) {
           if ($("div").is("#ajax_data")){
               DatatableRemoteAjaxDemo.init(); 
           }
        });
    });
JS;
//маркер конца строки, обязательно сразу, без пробелов и табуляции
$this->registerJs($script, yii\web\View::POS_READY);
\yii\widgets\Pjax::begin(['enablePushState'=>false, 'id'=>'pjax-box', 'options'=>['data-target'=>'box-all']]);
?>
<div class="m-grid__item m-grid__item--fluid m-wrapper">
    <div class="m-content">
        <div class="m-portlet m-portlet--mobile">
            <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                        <h3 class="m-portlet__head-text">
                            <?=$pageName?>
                        </h3>
                    </div>
                </div>
                <div class="m-portlet__head-tools">
                    <ul class="m-portlet__nav">
                        <li class="m-portlet__nav-item">
                            <div class="m-dropdown m-dropdown--inline m-dropdown--arrow m-dropdown--align-right m-dropdown--align-push" data-dropdown-toggle="hover" aria-expanded="true">
                                <a href="#" class="m-portlet__nav-link btn btn-lg btn-secondary  m-btn m-btn--icon m-btn--icon-only m-btn--pill  m-dropdown__toggle">
                                    <i class="la la-ellipsis-h m--font-brand"></i>
                                </a>
                                <div class="m-dropdown__wrapper">
                                    <span class="m-dropdown__arrow m-dropdown__arrow--right m-dropdown__arrow--adjust"></span>
                                    <div class="m-dropdown__inner">
                                        <div class="m-dropdown__body">
                                            <div class="m-dropdown__content">
                                                <ul class="m-nav">
                                                    <?php
                                                    if ($table == 'prices'){
                                                    ?>
                                                    <li class="m-nav__item">
                                                        <a href="/admin/prices/import" class="m-nav__link">
                                                            <i class="m-nav__link-icon flaticon-list-1"></i>
                                                            <span class="m-nav__link-text">
                                                                Импорт
                                                            </span>
                                                        </a>
                                                    </li>
                                                    <?php   } ?>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="m-portlet__body"> 
                <!--begin: Search Form -->
                <div class="m-form m-form--label-align-right m--margin-top-20 m--margin-bottom-30">
                    <div class="row align-items-center" style="margin-bottom: 30px;">
                        <div class="col-xl-12 order-2 order-xl-1">
                            <div class="form-group m-form__group row align-items-center">
                                <?php
                                foreach ($filters as $filter) {
                                ?>
                                <div class="col-md-3">
                                    <div class="m-form__group m-form__group--inline">
                                        <div class="m-form__label">
                                            <label>
                                                <?php echo $filter['title']?>:
                                            </label>
                                        </div>
                                        <div class="m-form__control">
                                            <select class="form-control m-bootstrap-select" data-row="<?php echo $filter['field']?>" data-toggle="filter-table">
                                                <option value="">
                                                    <?php echo Yii::t('app', 'Всі'); ?>
                                                </option>
                                                <?php
                                                $rows=$model->find()->select($filter['field'])->distinct()->all();
                                                foreach ($rows as $row){
                                                ?>
                                                <option value="<?=$row[$filter['field']]?>">
                                                    <?
                                                    if ($filter['table']['name']==null){
                                                        echo $filter['data'][$row[$filter['field']]];
                                                    } else {
                                                        $string = Yii::$app->db->createCommand("select * from " . $filter['table']['name'] . " where " . $filter['table']['value'] . "='" . $row[$filter['field']] . "'")->queryOne();
                                                        foreach ($filter['table']['text'] as $one) {
                                                            echo $string[$one] . ' ';
                                                        }
                                                    }
                                                    ?>
                                                </option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="d-md-none m--margin-bottom-10"></div>
                                </div>
                                <? } ?>
                            </div>
                        </div>
                    </div>
                    <div class="row align-items-center">
                        <div class="col-xl-6 order-2 order-xl-1">
                            <div class="form-group m-form__group row align-items-center">
                                <div class="col-md-12">
                                    <div class="m-input-icon m-input-icon--left">
                                        <input type="text" class="form-control m-input" placeholder="<?php echo Yii::t('app', 'Пошук...') ?>" id="m_form_search">
                                        <span class="m-input-icon__icon m-input-icon__icon--left">
                                            <span>
                                                <i class="la la-search"></i>
                                            </span>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-6 order-1 order-xl-2 m--align-right">
                            <a href="<?php echo \yii\helpers\Url::to('/admin/'.$table.'/add');?>" class="btn btn-primary m-btn m-btn--custom m-btn--icon m-btn--air m-btn--pill" data-toggle="load-page" data-word="<?php echo Yii::t('app', 'Зачекайте...') ?>">
                                <span>
                                    <i class="la la-plus"></i>
                                    <span>
                                        <?php echo Yii::t('app', 'Додати'); ?>
                                    </span>
                                </span>
                            </a>
                            <div class="m-separator m-separator--dashed d-xl-none"></div>
                        </div>
                    </div>
                </div>
                <!--end: Search Form -->
                <!--begin: Datatable -->
                <div class="m_datatable" id="ajax_data"></div>
                <!--end: Datatable -->
            </div>
        </div>
    </div>
</div>
<?php
\yii\widgets\Pjax::end();