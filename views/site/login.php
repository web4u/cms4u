<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = Yii::t('app', 'Вхід в систему управління');
$this->registerJsFile('assets/snippets/pages/user/login.js');
?>
<div class="m-grid__item m-grid__item--fluid m-grid m-grid--ver-desktop m-grid--desktop m-grid--tablet-and-mobile m-grid--hor-tablet-and-mobile m-login m-login--1 m-login--singin" id="m_login">
    <div class="m-grid__item m-grid__item--order-tablet-and-mobile-2 m-login__aside">
        <div class="m-stack m-stack--hor m-stack--desktop">
            <div class="m-stack__item m-stack__item--fluid">
                <div class="m-login__wrapper">
                    <div class="m-login__logo">
                        <a href="#">
                            <img src="<?=\yii\helpers\Url::to('/admin/assets/app/media/img/logos/logo-2.png') ?>">
                        </a>
                    </div>
                    <div class="m-login__signin">
                        <div class="m-login__head">
                            <h3 class="m-login__title">
                                <?php echo $this->title ?>
                            </h3>
                        </div>
                        <?php $form = ActiveForm::begin(['options'=>['class'=>'m-login__form m-form','fieldConfig' => ['options' => ['tag' => false]]]]); ?>
                            <div class="form-group m-form__group">
                                <?= $form->field($model, 'username')->textInput(['autofocus' => true,'class'=>'form-control m-input', 'placeholder'=>Yii::t('app', 'Логін')])->label(false) ?>
                            </div>
                            <div class="form-group m-form__group">
                                <?= $form->field($model, 'password')->passwordInput(['class'=>'form-control m-input m-login__form-input--last', 'placeholder'=>Yii::t('app', 'Пароль')])->label(false) ?>
                            </div>
                            <div class="row m-login__form-sub">
                                <div class="col m--align-left">
                                    <label class="m-checkbox m-checkbox--focus">
                                        <input type="checkbox" name="LoginForm[rememberMe]">
                                        <?php echo Yii::t('app', 'Запам\'ятати мене');?>
                                        <span></span>
                                    </label>
                                </div>
                                <div class="col m--align-right">
                                    <a href="javascript:;" id="m_login_forget_password" class="m-link">
                                        <?php echo Yii::t('app', 'Забули пароль?');?>
                                    </a>
                                </div>
                            </div>
                            <div class="m-login__form-action">
                                <button class="btn btn-focus m-btn m-btn--pill m-btn--custom m-btn--air">
                                    <?php echo Yii::t('app', 'Ввійти');?>
                                </button>
                            </div>
                        <?php ActiveForm::end(); ?>
                    </div>
                    <div class="m-login__signup">
                        <?php
                        \yii\widgets\Pjax::begin(['enablePushState'=>false]);
                        ?>
                        <div class="m-login__head">
                            <h3 class="m-login__title">
                                <?php
                                echo Yii::t('app', 'Реєстрація');
                                ?>
                            </h3>
                            <div class="m-login__desc">
                                <?php
                                echo Yii::t('app', 'Введіть ваші дані');
                                ?>
                            </div>
                        </div>
                        <?php $form = ActiveForm::begin(['action'=>'/admin/site/signup','options'=>['class'=>'m-login__form m-form','data-pjax'=>true,'fieldConfig' => ['options' => ['tag' => false]]]]); ?>
                            <div class="form-group m-form__group">
                                <?php
                                echo $form->field($sign, 'name')->textInput(['class'=>'form-control m-input','placeholder'=>Yii::t('app', 'Ваше ім\'я')])->label(false);
                                ?>
                            </div>
                            <div class="form-group m-form__group">
                                <?php
                                echo $form->field($sign, 'email')->textInput(['class'=>'form-control m-input','placeholder'=>'Email'])->label(false);
                                ?>
                            </div>
                        <div class="form-group m-form__group">
                                <?php
                                echo $form->field($sign, 'username')->textInput(['class'=>'form-control m-input','placeholder'=>Yii::t('app', 'Логін')])->label(false);
                                ?>
                            </div>
                            <div class="form-group m-form__group">
                                <?php
                                echo $form->field($sign, 'password')->passwordInput(['class'=>'form-control m-input','placeholder'=>Yii::t('app', 'Пароль')])->label(false);
                                ?>
                            </div>
                            <div class="form-group m-form__group">
                                <?php
                                echo $form->field($sign, 'password_repeat')->passwordInput(['class'=>'form-control m-input m-login__form-input--last','placeholder'=>Yii::t('app', 'Повторіть пароль')])->label(false);
                                ?>
                            </div>
                            <div class="m-login__form-action">
                                <button class="btn btn-focus m-btn m-btn--pill m-btn--custom m-btn--air">
                                    <?php
                                    echo Yii::t('app', 'Зареєстуватися');
                                    ?>
                                </button>
                                <button id="m_login_signup_cancel" class="btn btn-outline-focus  m-btn m-btn--pill m-btn--custom">
                                    <?php
                                    echo Yii::t('app', 'Відміна');
                                    ?>
                                </button>
                            </div>
                        <?php
                        ActiveForm::end();
                        \yii\widgets\Pjax::end();
                        ?>
                    </div>
                    <div class="m-login__forget-password">
                        <div class="m-login__head">
                            <h3 class="m-login__title">
                                Forgotten Password ?
                            </h3>
                            <div class="m-login__desc">
                                Enter your email to reset your password:
                            </div>
                        </div>
                        <form class="m-login__form m-form" action="">
                            <div class="form-group m-form__group">
                                <input class="form-control m-input" type="text" placeholder="Email" name="email" id="m_email" autocomplete="off">
                            </div>
                            <div class="m-login__form-action">
                                <button id="m_login_forget_password_submit" class="btn btn-focus m-btn m-btn--pill m-btn--custom m-btn--air">
                                    Request
                                </button>
                                <button id="m_login_forget_password_cancel" class="btn btn-outline-focus m-btn m-btn--pill m-btn--custom">
                                    Cancel
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="m-stack__item m-stack__item--center">
                <div class="m-login__account">
								<span class="m-login__account-msg">
									<?php echo Yii::t('app', 'Немає аккаунта?') ?>
								</span>
                    &nbsp;&nbsp;
                    <a href="javascript:;" id="m_login_signup" class="m-link m-link--focus m-login__account-link">
                        <?php echo Yii::t('app', 'Реєстрація') ?>
                    </a>
                </div>
            </div>
        </div>
    </div>
    <div class="m-grid__item m-grid__item--fluid m-grid m-grid--center m-grid--hor m-grid__item--order-tablet-and-mobile-1	m-login__content" style="background-image: url(/admin/assets/app/media/img//bg/bg-4.jpg)">
        <div class="m-grid__item m-grid__item--middle">
            <h3 class="m-login__welcome">
                <?php echo Yii::t('app', 'Ваша система управління'); ?>
            </h3>
            <p class="m-login__msg">
                <?php echo Yii::t('app', 'Розроблена, щоб покращити вам життя...'); ?>
            </p>
        </div>
    </div>
</div>
