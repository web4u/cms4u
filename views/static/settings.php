<?php
$this->title=Yii::t('app', 'Налаштування');
?>
<div class="m-grid__item m-grid__item--fluid m-wrapper">
    <div class="m-content">
        <?php
        if (isset($error)){
            ?>
            <div class="m-alert m-alert--icon alert alert-danger" role="alert">
                <div class="m-alert__icon">
                    <i class="flaticon-danger"></i>
                </div>
                <div class="m-alert__text">
                    <strong><?php echo Yii::t('app', 'Ви допустили помилки!') ?></strong>
                    <?
                    foreach ($error as $f){
                        for ($e=0;$e<count($f);$e++){
                            ?>
                            <div><?php echo $f[$e]?></div>
                            <?php
                        }
                    }
                    ?>
                </div>
            </div>
        <?php } ?>
        <div class="row">
            <div class="col-lg-12">
                <!--begin::Portlet-->
                <div class="m-portlet">
                    <div class="m-portlet__head">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title">
						<span class="m-portlet__head-icon m--hide">
						<i class="la la-gear"></i>
						</span>
                                <h3 class="m-portlet__head-text">
                                    <?php echo $this->title?>
                                </h3>
                            </div>
                        </div>
                    </div>
                    <!--begin::Form-->
                    <?php
                    $form = \yii\bootstrap\ActiveForm::begin(['enableAjaxValidation' => true,'options'=>['class'=>'m-form','data-pjax' => true,'fieldConfig' => ['options' => ['tag' => false]]]]); ?>
                    <div class="m-portlet__body">
                        <div class="m-form__section m-form__section--first">
                            <?php
                            foreach ($elements as $element) {
                                switch ($element['type']){
                                case 'input':
                                    ?>
                                    <div class="form-group m-form__group row">
                                        <label class="col-lg-3 col-form-label"><?php echo $element['label']?>:</label>
                                        <div class="col-lg-9">
                                            <input type="text" class="form-control m-input" name="Elements[<?php echo $element['id']?>]" value="<?php echo $element['value']?>">
                                        </div>
                                    </div>
                                <?php
                                break;

                                case 'textarea':
                                ?>
                                    <div class="form-group m-form__group row">
                                        <label class="col-lg-3 col-form-label"><?php echo $element['label']?>:</label>
                                        <div class="col-lg-9">
                                            <textarea class="form-control m-input" name="Elements[<?php echo $element['id']?>]" rows="3"><?php echo $element['value']?></textarea>
                                        </div>
                                    </div>
                                <?php
                                break;

                                case 'select':
                                ?>
                                    <div class="form-group m-form__group row">
                                        <label class="col-lg-3 col-form-label"><?php echo $element['label']?>:</label>
                                        <div class="col-lg-6">
                                            <select class="form-control select-custom" name="<?php echo $class?>[<?php echo $element['table']['field']?>]">
                                                <option value="">
                                                    <?php echo Yii::t('app', 'виберіть'); ?>
                                                </option>
                                                <?php
                                                $select=Yii::$app->db->createCommand("select * from ".$element['view']['table']['name']." order by id")->queryAll();
                                                foreach ($select as $option){
                                                    ?>
                                                    <option <?php if ($option[$element['view']['table']['value']]==$model[$element['table']['field']]) echo 'selected'; ?> value="<?=$option[$element['view']['table']['value']]?>">
                                                        <?php
                                                        foreach ($element['view']['table']['text'] as $option_text){
                                                            echo $option[$option_text].' ';
                                                        }
                                                        ?>
                                                    </option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                    </div>
                                    <script>
                                        $(document).on('pjax:success', function(e) {
                                            $(".select-custom").select2();
                                        });
                                    </script>
                                <?php
                                break;

                                case 'radio':
                                ?>
                                    <div class="m-form__group form-group row">
                                        <label class="col-lg-3 col-form-label"><?php echo $element['table']['title']?>:</label>
                                        <div class="col-lg-6 m-radio-list">
                                            <?php
                                            foreach ($element['view']['data'] as $k=>$v){
                                                $checked=$model[$element['table']['field']]==$k?'checked':'';
                                                ?>
                                                <label class="m-radio">
                                                    <input type="radio" name="<?php echo $class?>[<?php echo $element['table']['field']?>]" <?php echo $checked ?> value="<?php echo $k ?>"> <?php echo $v ?>
                                                    <span></span>
                                                </label>
                                            <?php } ?>
                                        </div>
                                    </div>
                                <?php
                                break;

                                case 'file':
                                ?>
                                    <div class="form-group m-form__group row">
                                        <label class="col-lg-3 col-form-label"><?php echo $element['label']?>:</label>
                                        <div class="col-lg-9">
                                            <div class="m-alert m-alert--icon m-alert--outline alert alert-info alert-dismissible fade show" role="alert">
                                                <div class="m-alert__icon">
                                                    <i class="la la-file-o"></i>
                                                </div>
                                                <div class="m-alert__text">
                                                    <strong><?php echo Yii::t('app', 'Доступні формати:') ?></strong> png, jpeg, jpg, gif
                                                </div>
                                                <button type="button" class="btn button-load btn-info" onclick="$(this).next().click();" style="margin-top: 20px;"><?php echo Yii::t('app', 'Вибрати файл') ?></button>
                                                <input type="file" data-maxsize="5" data-extend="png,jpeg,jpg,gif" class="hide" id="load-file-<?php echo $element['id']?>">
                                                <script>
                                                    $(function(){
                                                        $("#load-file-<?php echo $element['id']?>").jLoad({
                                                            beforeload: function () {
                                                                $("#load-file-<?php echo $element['id']?>").prev().addClass('m-loader m-loader--light m-loader--right');
                                                            },
                                                            onsuccess: function(res){
                                                                $("#box-to-files-<?php echo $element['id']?>").html('<div class="box-to-load-image"><i class="la la-trash"></i><img src="'+res+'" alt="" class="img-thumbnail"><input type="hidden" value="'+res+'" name="Elements[<?php echo $element['id']?>][]"></div>');
                                                                $("#load-file-<?php echo $element['id']?>").prev().removeClass('m-loader m-loader--light m-loader--right');
                                                            },
                                                            onerror: function(){
                                                                alert("<?php echo Yii::t('app', 'Не допустимий формат файлу або великий розмір файлу')?>")
                                                            }
                                                        });
                                                    });
                                                </script>
                                            </div>
                                            <div id="box-to-files-<?php echo $element['id']?>" class="clearfix alert-image" data-toggle="all-image">
                                                <?php
                                                if (isset($element['value'])){
                                                    $pics=json_decode($element['value']);
                                                    foreach ($pics as $pic){
                                                        ?>
                                                        <div class="box-to-load-image">
                                                            <i class="la la-trash"></i>
                                                            <img src="<?php echo $pic?>" alt="" class="img-thumbnail">
                                                            <input type="hidden" value="<?php echo $pic ?>" name="Elements[<?php echo $element['id']?>][]">
                                                        </div>
                                                        <?php
                                                    }
                                                }
                                                ?>

                                            </div>
                                        </div>
                                    </div>
                                <?php
                                break;

                                case 'editor':
                                ?>
                                    <div class="form-group m-form__group row">
                                        <label class="col-lg-3 col-form-label"><?php echo $element['label']?>:</label>
                                        <div class="col-lg-9">
                                            <textarea class="form-control m-input editor" name="Elements[<?php echo $element['id']?>]" rows="5"><?php echo $element['value']?></textarea>
                                        </div>
                                    </div>
                                    <script>
                                        $(document).on('pjax:success', function(e) {
                                            tinymce.remove();
                                            tinymce.init({
                                                selector: 'textarea.editor',
                                                height: 400,
                                                language: 'ru',
                                                clientOptions: {
                                                    plugins: [
                                                        "advlist autolink lists link charmap print preview anchor",
                                                        "searchreplace visualblocks code fullscreen",
                                                        "insertdatetime media table contextmenu paste code"
                                                    ],
                                                    'toolbar': [
                                                        "undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image code"
                                                    ]
                                                }
                                            });
                                        });
                                    </script>
                                    <?php
                                    break;
                                }
                            }
                            ?>
                        </div>
                    </div>
                    <div class="m-portlet__foot m-portlet__foot--fit">
                        <div class="m-form__actions m-form__actions">
                            <div class="row">
                                <div class="col-lg-3"></div>
                                <div class="col-lg-6">
                                    <button type="submit" data-toggle="load-page" data-word="<?php echo Yii::t('app', 'Зачекайте...') ?>" class="btn btn-success"><?php echo Yii::t('app', 'Зберегти') ?></button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php
                    \yii\bootstrap\ActiveForm::end();
                    ?>
                    <!--end::Form-->
                </div>
                <!--end::Portlet-->
            </div>
        </div>
    </div>
</div>