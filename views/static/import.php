<?php
$this->title=Yii::t('app', 'Импорт');
?>
<div class="m-grid__item m-grid__item--fluid m-wrapper">
    <div class="m-content">
        <?php
        if (isset($error)){
            ?>
            <div class="m-alert m-alert--icon alert alert-danger" role="alert">
                <div class="m-alert__icon">
                    <i class="flaticon-danger"></i>
                </div>
                <div class="m-alert__text">
                    <strong><?php echo Yii::t('app', 'Ви допустили помилки!') ?></strong>
                    <?
                    foreach ($error as $f){
                        for ($e=0;$e<count($f);$e++){
                            ?>
                            <div><?php echo $f[$e]?></div>
                            <?php
                        }
                    }
                    ?>
                </div>
            </div>
        <?php } ?>
        <div class="row">
            <div class="col-lg-12">
                <!--begin::Portlet-->
                <div class="m-portlet">
                    <div class="m-portlet__head">
                        <div class="m-portlet__head-caption">
                            <div class="m-portlet__head-title">
						<span class="m-portlet__head-icon m--hide">
						<i class="la la-gear"></i>
						</span>
                                <h3 class="m-portlet__head-text">
                                    <?php echo $this->title?>
                                </h3>
                            </div>
                        </div>
                    </div>
                    <!--begin::Form-->
                    <?php
                    $form = \yii\bootstrap\ActiveForm::begin(['enableAjaxValidation' => true,'options'=>['class'=>'m-form','data-pjax' => true,'fieldConfig' => ['options' => ['tag' => false]], 'enctype' => 'multipart/form-data']]); ?>
                    <div class="m-portlet__body">
                        <div class="m-form__section m-form__section--first">
                            <div class="form-group m-form__group row">
                                <label class="col-lg-3 col-form-label">Поставщик:</label>
                                <div class="col-lg-6">
                                    <select class="form-control select-custom" name="supplier">
                                        <option value="">
                                            <?php echo Yii::t('app', 'виберіть'); ?>
                                        </option>
                                        <?php
                                        $select = \backend\models\Suppliers::find()->all();
                                        foreach ($select as $option){
                                            ?>
                                            <option value="<?=$option['id']?>"><?=$option['name']?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group m-form__group row">
                                <label class="col-lg-3 col-form-label">Валюта:</label>
                                <div class="col-lg-6">
                                    <select class="form-control select-custom" name="currency">
                                        <option value="">
                                            <?php echo Yii::t('app', 'виберіть'); ?>
                                        </option>
                                        <?php
                                        $select = \backend\models\Kurs::find()->all();
                                        foreach ($select as $option){
                                            ?>
                                            <option value="<?=$option['currency_from']?>"><?=$option['currency_from']?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                            <?php
                            $abetka = ['','A','B','C','D','E','F','G','H','I','J'];
                            ?>
                            <div class="form-group m-form__group row">
                                <label class="col-lg-3 col-form-label">Поле с производителем:</label>
                                <div class="col-lg-6">
                                    <select class="form-control select-custom" name="Row[brand]">
                                        <?php
                                        for ($i=1;$i<=10;$i++){
                                            ?>
                                            <option value="<?=$abetka[$i]?>"><?=$i?></option>
                                            <?php
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group m-form__group row">
                                <label class="col-lg-3 col-form-label">Поле с артиклом:</label>
                                <div class="col-lg-6">
                                    <select class="form-control select-custom" name="Row[article]">
                                        <?php
                                        for ($i=1;$i<=10;$i++){
                                            ?>
                                            <option value="<?=$abetka[$i]?>"><?=$i?></option>
                                            <?php
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group m-form__group row">
                                <label class="col-lg-3 col-form-label">Поле с названием:</label>
                                <div class="col-lg-6">
                                    <select class="form-control select-custom" name="Row[name]">
                                        <?php
                                        for ($i=1;$i<=10;$i++){
                                            ?>
                                            <option value="<?=$abetka[$i]?>"><?=$i?></option>
                                            <?php
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group m-form__group row">
                                <label class="col-lg-3 col-form-label">Поле с ценой:</label>
                                <div class="col-lg-6">
                                    <select class="form-control select-custom" name="Row[price]">
                                        <?php
                                        for ($i=1;$i<=10;$i++){
                                            ?>
                                            <option value="<?=$abetka[$i]?>"><?=$i?></option>
                                            <?php
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group m-form__group row">
                                <label class="col-lg-3 col-form-label">Поле с наличием:</label>
                                <div class="col-lg-6">
                                    <select class="form-control select-custom" name="Row[avalible]">
                                        <?php
                                        for ($i=1;$i<=10;$i++){
                                            ?>
                                            <option value="<?=$abetka[$i]?>"><?=$i?></option>
                                            <?php
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group m-form__group row">
                                <label class="col-lg-3 col-form-label">Файл:</label>
                                <div class="col-lg-6">
                                    <input type="file" name="file">
                                </div>
                            </div>
                            <div class="form-group m-form__group row">
                                <label class="col-lg-3 col-form-label">Начать с строки:</label>
                                <div class="col-lg-9">
                                    <input type="text" class="form-control m-input" name="start" value="1">
                                </div>
                            </div>
                            <script>
                                $(document).on('pjax:success', function(e) {
                                    $(".select-custom").select2();
                                });
                            </script>

                        </div>
                    </div>
                    <div class="m-portlet__foot m-portlet__foot--fit">
                        <div class="m-form__actions m-form__actions">
                            <div class="row">
                                <div class="col-lg-3"></div>
                                <div class="col-lg-6">
                                    <button type="submit" data-toggle="load-page" data-word="<?php echo Yii::t('app', 'Зачекайте...') ?>" class="btn btn-success"><?php echo Yii::t('app', 'Зберегти') ?></button>
                                    <a href="<?php echo \yii\helpers\Url::to('/admin/'.$table)?>" class="btn btn-secondary" data-toggle="load-page" data-word="<?php echo Yii::t('app', 'Зачекайте...') ?>"><?php echo Yii::t('app', 'Відміна') ?></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php
                    \yii\bootstrap\ActiveForm::end();
                    ?>
                    <!--end::Form-->
                </div>
                <!--end::Portlet-->
            </div>
        </div>
    </div>
</div>