<div class="m-section__content">
    <div class="m-portlet__body">
        <table id="all-position" class="table m-table m-table--head-separator-danger">
            <thead>
            <tr>
                <th>Товар</th>
                <th>Артикуль</th>
                <th>К-во</th>
                <th>Цена</th>
                <th></th>
            </tr>
            </thead>
            <tbody>
            <?php
            $carts = \common\models\Carts::find()->where(['status'=>$id])->all();
            foreach ($carts as $cart) {
                $item = \common\models\Products::findOne($cart['item']);
                ?>
                <tr>
                    <td><?=$item['name']?><input type="hidden" name="Carts[item][]" value="<?=$cart['item']?>"><input type="hidden" name="Carts[id][]" value="<?=$cart['id']?>"></td>
                    <td><?=$item['article']?><input type="hidden" name="Carts[article][]" value="<?=$item['article']?>"></td>
                    <td><?=$cart['count']?><input type="hidden" name="Carts[count][]" value="<?=$cart['count']?>"></td>
                    <td><?=$cart['price']?><input type="hidden" name="Carts[price][]" value="<?=$cart['price']?>"></td>
                    <td>
                        <button onclick="$(this).closest('tr').remove();" class="btn btn-danger">Удалить</button>
                    </td>
                </tr>
            <?php } ?>
            </tbody>
        </table>
    </div>
    <hr>
    <div class="m-portlet__body">
        <div class="m-form__section m-form__section--first">
            <div class="form-group m-form__group row">
                <label class="col-lg-3 col-form-label">Товар:</label>
                <div class="col-lg-9">
                    <select style="width: 100%" id="select-product" class="form-control select-custom" data-name="Carts[item]">
                        <option value="">
                            <?php echo Yii::t('app', 'виберіть'); ?>
                        </option>
                        <?php
                        $items = \common\models\Products::find()->all();
                        foreach ($items as $option){
                            ?>
                            <option value="<?=$option['id']?>"><?=$option['name']?></option>
                        <?php } ?>
                    </select>
                </div>
            </div>
            <div class="form-group m-form__group row">
                <label class="col-lg-3 col-form-label">Количество:</label>
                <div class="col-lg-9">
                    <input type="text" class="form-control m-input" data-name="Carts[count]" value="1">
                </div>
            </div>
            <div class="form-group m-form__group row">
                <label class="col-lg-3 col-form-label">Aртикуль:</label>
                <div class="col-lg-9">
                    <input type="text" class="form-control m-input" data-name="Carts[article]" value="">
                </div>
            </div>
            <div class="form-group m-form__group row">
                <label class="col-lg-3 col-form-label">Цена:</label>
                <div class="col-lg-9">
                    <input type="text" class="form-control m-input" data-name="Carts[price]" value="">
                </div>
            </div>
            <script>
                $(document).on('pjax:success', function(e) {
                    $(".select-custom").select2();
                });
            </script>

            <div class="row">
                <div class="col-lg-3"></div>
                <div class="col-lg-6">
                    <button type="button" id="add-cart-item" data-toggle="load-page" data-word="<?php echo Yii::t('app', 'Зачекайте...') ?>" class="btn btn-success"><?php echo Yii::t('app', 'Добавить') ?></button>
                </div>
            </div>
        </div>
    </div>
</div>