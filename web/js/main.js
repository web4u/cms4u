$(function () {
    $('[data-toggle=load-page]').on('click', function () {
        mApp.blockPage({
            overlayColor: '#000000',
            type: 'loader',
            state: 'success',
            message: $(this).attr('data-word')
        });
    });

    $(document).on('pjax:success', function (e) {
        // if ($('#pjax-box').attr('data-target')=='box-all'){
        //     setTimeout(function () {
        //         $('#pjax-box').attr('data-target', 'box-edit');
        //     }, 100);
        // }
        mApp.unblock();
    });

    // var socket = io('web4u.in.ua:4444');
    // socket.on('connect', function () {
    //     socket.emit('client', window.location.hostname, $(".m-topbar__userpic img").attr('src'), $(".m-card-user__name").text());
    //     console.log('connect');
    // });
    //
    // socket.on('new admin sms', function (data) {
    //     console.log(data);
    // });
    //
    // socket.emit('show', 'cook.w4u.pp.ua');
    //
    // socket.on('all sms', function (data) {
    //     console.log(data);
    // });

    $(document).on('click', '.box-to-load-image i', function () {
        $(this).closest('.box-to-load-image').remove();
    });

    if ($("table").is("#html_table")) {
        var DatatableHtmlTableDemo = function () {
            var e = function () {
                var e = $("#html_table").mDatatable({
                    columns: [{
                        field: "Action",
                        width: 100,
                        title: 'Дествия',
                        sortable: false,
                        template: function (row) {
                            return '<a href="/admin/texts/edit/' + row.ID + '" class="m-portlet__nav-link btn m-btn m-btn--hover-accent m-btn--icon m-btn--icon-only m-btn--pill" title="Edit details"><i class="la la-edit"></i></a>';
                        }
                    }]
                }), a = e.getDataSourceQuery();
                $("#m_form_search").on("keyup", function (a) {
                    e.search($(this).val().toLowerCase())
                }).val(a.generalSearch)
            };
            return {
                init: function () {
                    e()
                }
            }
        }();
        jQuery(document).ready(function () {
            DatatableHtmlTableDemo.init()
        });
    }

    $(".m-menu__toggle").click(function (e) {
        e.preventDefault();
    });

    $(document).on('click', '#add-cart-item', function () {
        var item = $("select[data-name='Carts[item]']").val();
        var article = $("input[data-name='Carts[article]']").val();
        var count = $("input[data-name='Carts[count]']").val();
        var price = $("input[data-name='Carts[price]']").val();
        var name = $("select[data-name='Carts[item]'] option:selected").text();

        $("#all-position").append('<tr>\n' +
            '                                                <td>'+name+'<input type="hidden" name="Carts[item][]" value="'+item+'"><input type="hidden" name="Carts[id][]" value=""></td>\n' +
            '                                                <td>'+article+'</td>\n' +
            '                                                <td>'+count+'<input type="hidden" name="Carts[count][]" value="'+count+'"></td>\n' +
            '                                                <td>'+price+'<input type="hidden" name="Carts[price][]" value="'+price+'"></td>\n' +
            '                                                <td>\n' +
            '                                                    <button onclick="$(this).closest(\'tr\').remove();" class="btn btn-danger">Удалить</button>\n' +
            '                                                </td>\n' +
            '                                            </tr>');

        $("input[data-name='Carts[article]']").val('');
        $("input[data-name='Carts[count]']").val('');
        $("input[data-name='Carts[price]']").val('');
    });
    
    $(document).on('change', '#select-product', function () {
        $.ajax({
            url: '/admin/helper/info-product',
            type: 'post',
            data: {id: $(this).val()},
            dataType: 'json',
            success: function (res) {
                $("input[data-name='Carts[article]']").val(res.article);
                $("input[data-name='Carts[price]']").val(res.price);
            }
        })
    })
});