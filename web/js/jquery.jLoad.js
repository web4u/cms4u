(function($) {
    $.fn.jLoad = function(options) {
        options = $.extend({
            path: '/source/sites/',
            onsuccess: function(res){
                console.log(res);
            },
            beforeload: false,
            onerror: false
        }, options);
        $(this).change(function(evt) {
            var files = evt.target.files;
            var max_size=$(this).attr('data-maxsize');
            var ext=($(this).attr('data-extend')).split(',');
            for (var i = 0, f; f = files[i]; i++) {
                renderImage(f, max_size, ext);
            }
        });

        function renderImage(file, max_size, ext) {
            var reader = new FileReader();
            reader.onload = function(event) {
                var size=(event.total/1024)/1024;
                var type=(event.target.result).split(';');
                type=type[0].split('/');
                if (size>max_size){
                    if (options.onerror!==false){
                        options.onerror();
                    }
                } else if (ext.indexOf(type[1])==-1){
                    if (options.onerror!==false){
                        options.onerror();
                    }
                } else {
                    if (options.beforeload!==false){
                        options.beforeload();
                    }
                    the_url = event.target.result;
                    $.ajax({
                        url: '/admin/helper/upload',
                        type: 'post',
                        data: 'tmp=' + the_url + '&name=' + file.name + '&folder=' + options.path,
                        success: function (res) {
                            options.onsuccess(res);
                        },
                        error: function () {
                            if (options.onerror!==false){
                                options.onerror();
                            }
                        }
                    })
                }
            }
            // РєРѕРіРґР° С„Р°Р№Р» СЃС‡РёС‚С‹РІР°РµС‚СЃСЏ РѕРЅ Р·Р°РїСѓСЃРєР°РµС‚ СЃРѕР±С‹С‚РёРµ OnLoad.
            reader.readAsDataURL(file);
        }

        return this;
    };
})(jQuery);