<?php

namespace backend\assets;

use yii\web\AssetBundle;

/**
 * Main backend application asset bundle.
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'assets/vendors/base/vendors.bundle.css',
        'assets/demo/default/base/style.bundle.css',
        'js/ui/jquery-ui.min.css',
        'css/site.css',
    ];
    public $js = [
        'js/ui/jquery-ui.min.js',
        'js/locales/bootstrap-datepicker.ru.js',
        'assets/demo/default/base/scripts.bundle.js',
        'assets/vendors/custom/fullcalendar/fullcalendar.bundle.js',
        'js/socket.io.js',
        'js/jquery.jLoad.js',
        'js/ckeditor/ckeditor.js',
        'js/main.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
    ];
}
