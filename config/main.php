<?php
$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);


return [
    'id' => 'app-backend',
    'basePath' => dirname(__DIR__),
    'controllerNamespace' => 'backend\controllers',
    'bootstrap' => ['log'],
    'modules' => [],
    'language' => 'ru-RU',
    'homeUrl'=>'/admin/',
    'components' => [
        'request' => [
            'csrfParam' => '_csrf-backend',
            'baseUrl'=>'/admin'
        ],
        'i18n' => [
            'translations' => [
                '*' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@app/messages',
                    'fileMap' => [
                        'app'       => 'app.php',
                    ],
                ],
            ],
        ],
        'user' => [
            'identityClass' => 'backend\models\User',
            'enableAutoLogin' => true,
            'identityCookie' => ['name' => '_identity-backend', 'httpOnly' => true],
            'loginUrl' => ['login'],
        ],
        'session' => [
            // this is the name of the session cookie used for login on the backend
            'name' => 'advanced-backend',
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
                'login'=>'site/login',
                'settings'=>'static/settings',
                'texts/<id:\d+>'=>'static/texts',
                '<table:[0-9a-zA-Z_-]+>'=>'site/all',
                '<table:[0-9a-zA-Z_-]+>/<id:\d+>/<sub:[0-9a-zA-Z_-]+>'=>'site/subs',
                '<table:[0-9a-zA-Z_-]+>/copy/<id:[0-9]+>'=>'static/copy',
                '<table:[0-9a-zA-Z_-]+>/up/<id:[0-9]+>'=>'static/up',
                '<table:[0-9a-zA-Z_-]+>/down/<id:[0-9]+>'=>'static/down',
                '<table:[0-9a-zA-Z_-]+>/add'=>'site/add',
                '<table:[0-9a-zA-Z_-]+>/save'=>'site/save',
                '<table:[0-9a-zA-Z_-]+>/edit/<id:[0-9]+>'=>'site/edit',
                '<table:[0-9a-zA-Z_-]+>/delete/<id:[0-9]+>'=>'site/delete',
                '<table:[0-9a-zA-Z_-]+>/elements/<id:[0-9]+>'=>'static/elements',
                '<table:[0-9a-zA-Z_-]+>/list_cart/<id:[0-9]+>'=>'static/list-cart',
                '<table:[0-9a-zA-Z_-]+>/save-elements/<id:[0-9]+>'=>'static/save-elements',
            ],
        ],
        'assetManager' => [
            'bundles' => [
                'yii\web\JqueryAsset' => [
                    'js'=>[]
                ],
            ],
        ],
    ],
    'params' => $params,
];
