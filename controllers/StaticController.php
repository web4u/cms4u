<?php

namespace backend\controllers;

use backend\models\Carts;
use backend\models\Elements;
use backend\models\Helper;
use backend\models\Kurs;
use backend\models\Prices;
use backend\models\Settings;
use backend\models\SignupForm;
use common\models\Texts;
use common\models\Translits;
use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\web\UploadedFile;

/**
 * Site controller
 */
class StaticController extends Controller
{
    public function beforeAction($action)
    {
        if ($action->id == 'upload') {
            $this->enableCsrfValidation = false;
        }

        return parent::beforeAction($action);
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['elements', 'save-elements', 'settings', 'texts', 'copy', 'up', 'down'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionElements($table, $id)
    {
        $elements = Elements::find()->where(['page_id' => $id])->all();
        return $this->renderPartial('elements', [
            'elements' => $elements,
            'id' => $id,
            'table' => $table
        ]);
    }

    public function actionSettings()
    {
        if (Yii::$app->request->post()){
            foreach (Yii::$app->request->post('Elements') as $key=>$value){
                $el=Settings::findOne($key);
                if (is_array($value)){
                    $value=json_encode($value);
                }
                $el->value=$value;
                $el->save();
            }
        }
        $elements=Settings::find()->all();
        return $this->render('settings', [
            'elements' => $elements,
        ]);
    }

    public function actionSaveElements($table, $id){
        foreach (Yii::$app->request->post('Elements') as $key=>$value){
            $el=Elements::findOne($key);
            if ($el['type']=='file'){
                $value=json_encode($value);
            }
            $el->value=$value;
            $el->save();
        }

        /*** -= Мультимовність =- ***/
        if (Yii::$app->request->post('Translits')){
            foreach (Yii::$app->request->post('Translits') as $key=>$value){
                foreach ($value as $sub_key=>$sub_value){
                    $tr = Translits::find()->where(['table'=>'elements', 'row'=>'value', 'element'=>$key, 'lang'=>$sub_key])->one();
                    if (empty($tr)){
                        $tr = new Translits();
                        $tr->table = 'elements';
                        $tr->row = 'value';
                        $tr->element = $key;
                        $tr->lang = $sub_key;
                        $tr->text = $sub_value;
                        $tr->save();
                    } else {
                        $tr->text = $sub_value;
                        $tr->save();
                    }
                }
            }
        }

        $table_origin = $table;//запамятовуємо оригінальну назву
        $table = explode('_', $table);
        if (empty($table[1])) {
            $table_real = ucfirst($table[0]);
        } else {
            $table_real = '';
            foreach ($table as $tab) {
                $table_real .= ucfirst($tab);
            }
        }
        $class = '\common\models\\' . $table_real;

        //Створюємо модель
        $model = new $class();

        //Вибираємо дані для таблиці і стовпці, які треба фільтрувати
        $filter = [];
        foreach ($model->rows() as $line) {
            if ($line['view']['display']) {
                $columns[] = $line['table'];
                if ($line['view']['filter']) {
                    $filter[] = [
                        'field' => $line['table']['field'],
                        'title' => $line['table']['title'],
                        'table' => $line['view']['table']
                    ];
                }
            }
        }
        $this->viewPath='@app/views/site';
        return $this->render('all', [
            'table' => $table_origin,
            'pageName' => $model::$pageName,
            'columns' => json_encode($columns),
            'filters' => $filter,
            'model' => $model
        ]);
    }

    public function actionTexts($id)
    {
        $texts = Texts::find()->where(['parent'=>$id])->all();
        return $this->render('texts', compact('texts'));
    }

    public function actionCopy($table, $id){
        $table_origin = $table;//запамятовуємо оригінальну назву
        $table = explode('_', $table);
        if (empty($table[1])) {
            $table_real = ucfirst($table[0]);
        } else {
            $table_real = '';
            foreach ($table as $tab) {
                $table_real .= ucfirst($tab);
            }
        }
        $class = '\common\models\\' . $table_real;

        $old = $class::findOne($id);
        $copy = new $class();
        $copy->attributes = $old->attributes;
        if ($table_origin=='products'){
            $copy->catalog = $old['catalog'];
        }
        $copy->save();

        //Створюємо модель
        $model = new $class();

        //Вибираємо дані для таблиці і стовпці, які треба фільтрувати
        $filter = [];
        foreach ($model->rows() as $line) {
            if ($line['view']['display']) {
                $columns[] = $line['table'];
                if ($line['view']['filter']) {
                    $filter[] = [
                        'field' => $line['table']['field'],
                        'title' => $line['table']['title'],
                        'table' => $line['view']['table']
                    ];
                }
            }
        }
        $this->viewPath='@app/views/site';
        return $this->render('all', [
            'table' => $table_origin,
            'pageName' => $model::$pageName,
            'columns' => json_encode($columns),
            'filters' => $filter,
            'model' => $model
        ]);
    }

    public function actionUp($table, $id){
        $table_origin = $table;//запамятовуємо оригінальну назву
        $table = explode('_', $table);
        if (empty($table[1])) {
            $table_real = ucfirst($table[0]);
        } else {
            $table_real = '';
            foreach ($table as $tab) {
                $table_real .= ucfirst($tab);
            }
        }
        $class = '\common\models\\' . $table_real;

        $active = $class::findOne($id);
        $up = $class::find()->where(['<', 'id', $id])->orderBy('id desc')->one();
        if (!empty($up)) {
            $active->id = $up['id'];
            $up->id = 0;
            $up->save();
            $active->save();
            $up->id = $id;
            $up->save();
        }

        //Створюємо модель
        $model = new $class();

        //Вибираємо дані для таблиці і стовпці, які треба фільтрувати
        $filter = [];
        foreach ($model->rows() as $line) {
            if ($line['view']['display']) {
                $columns[] = $line['table'];
                if ($line['view']['filter']) {
                    $filter[] = [
                        'field' => $line['table']['field'],
                        'title' => $line['table']['title'],
                        'table' => $line['view']['table']
                    ];
                }
            }
        }
        $this->viewPath='@app/views/site';
        return $this->render('all', [
            'table' => $table_origin,
            'pageName' => $model::$pageName,
            'columns' => json_encode($columns),
            'filters' => $filter,
            'model' => $model
        ]);
    }

    public function actionDown($table, $id){
        $table_origin = $table;//запамятовуємо оригінальну назву
        $table = explode('_', $table);
        if (empty($table[1])) {
            $table_real = ucfirst($table[0]);
        } else {
            $table_real = '';
            foreach ($table as $tab) {
                $table_real .= ucfirst($tab);
            }
        }
        $class = '\common\models\\' . $table_real;

        $active = $class::findOne($id);
        $up = $class::find()->where(['>', 'id', $id])->orderBy('id')->one();
        if (!empty($up)) {
            $active->id = $up['id'];
            $up->id = 0;
            $up->save();
            $active->save();
            $up->id = $id;
            $up->save();
        }

        //Створюємо модель
        $model = new $class();

        //Вибираємо дані для таблиці і стовпці, які треба фільтрувати
        $filter = [];
        foreach ($model->rows() as $line) {
            if ($line['view']['display']) {
                $columns[] = $line['table'];
                if ($line['view']['filter']) {
                    $filter[] = [
                        'field' => $line['table']['field'],
                        'title' => $line['table']['title'],
                        'table' => $line['view']['table']
                    ];
                }
            }
        }
        $this->viewPath='@app/views/site';
        return $this->render('all', [
            'table' => $table_origin,
            'pageName' => $model::$pageName,
            'columns' => json_encode($columns),
            'filters' => $filter,
            'model' => $model
        ]);
    }
}
