<?php

namespace backend\controllers;

use backend\models\Helper;
use backend\models\ProductFilters;
use backend\models\ProductPrice;
use backend\models\SignupForm;
use backend\models\User;
use common\models\Lawyers;
use common\models\Translits;
use Yii;
use yii\bootstrap\ActiveForm;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\LoginForm;
use yii\web\Response;
use common\models\Carts;

/**
 * Site controller
 */
class SiteController extends Controller
{
    public function beforeAction($action)
    {
        if ($action->id == 'show-all') {
            $this->enableCsrfValidation = false;
        }

        return parent::beforeAction($action);
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['login', 'error', 'signup', 'tmp'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['logout', 'index', 'all', 'show-all', 'add', 'edit', 'save', 'delete', 'subs'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * Login action.
     *
     * @return string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new \backend\models\LoginForm();
        $sign = new SignupForm();
        if ($model->load(Yii::$app->request->post())) {
            if ($model->login()) {
                return $this->redirect('/admin');
            } else {
                $this->layout = 'login';
                return $this->render('login', [
                    'model' => $model,
                    'sign' => $sign
                ]);
            }
        } else {
            $this->layout = 'login';
            return $this->render('login', [
                'model' => $model,
                'sign' => $sign
            ]);
        }
    }

    /**
     * Реєстрація
     *
     * @return string|\yii\web\Response
     */
    public function actionSignup()
    {
        $model = new SignupForm();
        if ($model->load(Yii::$app->request->post())) {
            if ($user = $model->signup()) {
                return Yii::t('app', 'Дякуємо за реєстрацію! Ви зможете використовувати систему після одоброення вашого акаунта адміністратором');
            } else {
                return $this->renderPartial('_signup', [
                    'model' => $model,
                ]);
            }
        } else {
            return $this->renderPartial('_signup', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Logout action.
     *
     * @return string
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    public function actionAll($table)
    {
        $table_origin = $table;//запамятовуємо оригінальну назву
        $table = explode('_', $table);
        if (empty($table[1])) {
            $table_real = ucfirst($table[0]);
        } else {
            $table_real = '';
            foreach ($table as $tab) {
                $table_real .= ucfirst($tab);
            }
        }
        $class = '\common\models\\' . $table_real;

        //Створюємо модель
        $model = new $class();

        //Вибираємо дані для таблиці і стовпці, які треба фільтрувати
        $filter = [];
        foreach ($model->rows() as $line) {
            if ($line['view']['display']) {
                $line['table']['title'] = $model::attributeLabels()[$line['table']['field']];
                $columns[] = $line['table'];
                if ($line['view']['filter']) {
                    $filter[] = [
                        'field' => $line['table']['field'],
                        'title' => $model::attributeLabels()[$line['table']['field']],
                        'table' => isset($line['view']['table'])?$line['view']['table']:null,
                        'data' => isset($line['view']['data'])?$line['view']['data']:null
                    ];
                }
            }
        }

        return $this->render('all', [
            'table' => $table_origin,
            'pageName' => $model::$pageName,
            'columns' => json_encode($columns),
            'filters' => $filter,
            'model' => $model
        ]);
    }


    public function actionShowAll()
    {
        if (!isset($_POST['datatable']['pagination']['sort'])) {
            $table = Yii::$app->request->get('table');
            $table_origin = $table;//запамятовуємо оригінальну назву
            $table = explode('_', $table);
            if (empty($table[1])) {
                $table_real = ucfirst($table[0]);
            } else {
                $table_real = '';
                foreach ($table as $tab) {
                    $table_real .= ucfirst($tab);
                }
            }
            $class = '\common\models\\' . $table_real;

            $model = new $class();

            $model_rows=$model::rows();

            $where = [];

            if (isset($_GET['filter'])){
                foreach (json_decode($_GET['filter']) as $fkey=>$fval){
                    $where[$fkey]=$fval;
                }
            }

            if (isset($_POST['datatable']['sort']['sort'])) {
                $res = $model->find()->where($where)->orderBy(Yii::$app->request->post('datatable')['sort']['field'] . ' ' . Yii::$app->request->post('datatable')['sort']['sort'])->limit(10)->all();
            } else {
                $res = $model->find()->where($where)->where($where)->limit(10)->all();
            }

            $allItems = $model->find()->count();

            $data = [
                'meta' => [
                    'page' => 1,
                    'pages' => ceil($allItems / 10),
                    'perpage' => 10,
                    'total' => $allItems,
                    'sort' => isset(Yii::$app->request->post('datatable')['sort']['sort']) ? Yii::$app->request->post('datatable')['sort']['sort'] : 'desc',
                    'field' => isset(Yii::$app->request->post('datatable')['sort']['field']) ? Yii::$app->request->post('datatable')['sort']['field'] : 'id'
                ]
            ];

            foreach ($res as $row) {
                $arr = [];
                foreach ($model_rows as $line){
                    if ($line['view']['display']) {
                        if ($line['table']['field'] == 'icon' || $line['table']['field'] == 'image' || $line['table']['field'] == 'img' || $line['table']['field'] == 'photo') {
                            $pic = json_decode($row[$line['table']['field']]);
                            $arr[$line['table']['field']] = '<img src="' . $pic[0] . '" style="max-width:100px">';
                        } else if ($line['view']['type']=='radio'){
                            $arr[$line['table']['field']] = $line['view']['data'][$row[$line['table']['field']]];
                        } else if ($line['view']['type']=='select'){
                            $arr[$line['table']['field']] = '';
                            $info=Yii::$app->db->createCommand("select * from ".$line['view']['table']['name']." where ".$line['view']['table']['value']."='".$row[$line['table']['field']]."'")->queryOne();
                            foreach ($line['view']['table']['text'] as $line_one) {
                                $arr[$line['table']['field']] .= $info[$line_one].' ';
                            }
                        } else {
                            $arr[$line['table']['field']] = $row[$line['table']['field']];
                        }
                    }
                }
                $data['data'][] = $arr;
            }
        } else {
            $table = Yii::$app->request->get('table');
            $table_origin = $table;//запамятовуємо оригінальну назву
            $table = explode('_', $table);
            if (empty($table[1])) {
                $table_real = ucfirst($table[0]);
            } else {
                $table_real = '';
                foreach ($table as $tab) {
                    $table_real .= ucfirst($tab);
                }
            }
            $class = '\common\models\\' . $table_real;

            $model = new $class();

            $model_rows=$model::rows();

            $where = ['AND'];
            if (isset($_POST['datatable']['query'])) {
                foreach (Yii::$app->request->post('datatable')['query'] as $key => $value) {
                    if ($value!='') {
                        if ($key != 'table') {
                            if ($key != 'generalSearch') {
                                $where[] = [$key => $value];
                            } else {
                                $sub_where = ['OR'];
                                foreach ($model->rows() as $field) {
                                    $sub_where[] = ['LIKE', $field['table']['field'], $value . '%', false];
                                }
                                $where[] = $sub_where;
                            }
                        }
                    }
                }
            }

            $res = $model->find()->where($where)->limit(Yii::$app->request->post('datatable')['pagination']['page']*Yii::$app->request->post('datatable')['pagination']['perpage'])->offset((Yii::$app->request->post('datatable')['pagination']['page']-1)*Yii::$app->request->post('datatable')['pagination']['perpage'])->all();

            $allItems = $model->find()->where($where)->count();

            $data = [
                'meta' => [
                    'page' => Yii::$app->request->post('datatable')['pagination']['page'],
                    'pages' => $allItems/10,
                    'perpage' => Yii::$app->request->post('datatable')['pagination']['perpage'],
                    'total' => $allItems,
                    'sort' => Yii::$app->request->post('datatable')['sort']['sort'],
                    'field' => Yii::$app->request->post('datatable')['sort']['field']
                ]
            ];

            foreach ($res as $row) {
                $arr = [];
                foreach ($model_rows as $line){
                    if ($line['view']['display']) {
                        if ($line['table']['field'] == 'icon' || $line['table']['field'] == 'image' || $line['table']['field'] == 'img' || $line['table']['field'] == 'photo') {
                            $pic = json_decode($row[$line['table']['field']]);
                            $arr[$line['table']['field']] = '<img src="' . $pic[0] . '" style="max-width:100px">';
                        } else if ($line['view']['type']=='radio'){
                            $arr[$line['table']['field']] = $line['view']['data'][$row[$line['table']['field']]];
                        } else if ($line['view']['type']=='select'){
                            $arr[$line['table']['field']] = '';
                            $info=Yii::$app->db->createCommand("select * from ".$line['view']['table']['name']." where ".$line['view']['table']['value']."='".$row[$line['table']['field']]."'")->queryOne();
                            foreach ($line['view']['table']['text'] as $line_one) {
                                $arr[$line['table']['field']] .= $info[$line_one].' ';
                            }
                        } else {
                            $arr[$line['table']['field']] = $row[$line['table']['field']];
                        }
                    }
                }
                $data['data'][] = $arr;
            }
        }

        return json_encode($data);
    }

    /**
     * Додавання елементу
     */
    public function actionAdd($table)
    {
        $table_origin = $table;//запамятовуємо оригінальну назву
        $table = explode('_', $table);
        if (empty($table[1])) {
            $table_real = ucfirst($table[0]);
        } else {
            $table_real = '';
            foreach ($table as $tab) {
                $table_real .= ucfirst($tab);
            }
        }
        $class = '\common\models\\' . $table_real;

        $model = new $class();

        if (Yii::$app->request->get('_pjax')) {
            return $this->renderPartial('add_edit', [
                'pageName' => Yii::t('app', 'Додати новий елемент'),
                'model' => $model,
                'table' => $table_origin,
                'class' => $table_real
            ]);
        } else {
            return $this->render('add_edit', [
                'pageName' => Yii::t('app', 'Додати новий елемент'),
                'model' => $model,
                'table' => $table_origin,
                'class' => $table_real
            ]);
        }
    }

    /**
     * Редагування елементу
     */
    public function actionEdit($table, $id)
    {
        $table_origin = $table;//запамятовуємо оригінальну назву
        $table = explode('_', $table);
        if (empty($table[1])) {
            $table_real = ucfirst($table[0]);
        } else {
            $table_real = '';
            foreach ($table as $tab) {
                $table_real .= ucfirst($tab);
            }
        }
        $class = '\common\models\\' . $table_real;

        $model = $class::findOne($id);

        if (Yii::$app->request->get('_pjax')) {
            return $this->renderPartial('add_edit', [
                'pageName' => Yii::t('app', 'Редагувати елемент'),
                'model' => $model,
                'table' => $table_origin,
                'id' => $id,
                'class' => $table_real
            ]);
        } else {
            return $this->render('add_edit', [
                'pageName' => Yii::t('app', 'Редагувати елемент'),
                'model' => $model,
                'table' => $table_origin,
                'id' => $id,
                'class' => $table_real
            ]);
        }
    }

    /**
     * Видалення елементу
     */
    public function actionDelete($table, $id)
    {
        $table_origin = $table;//запамятовуємо оригінальну назву
        $table = explode('_', $table);
        if (empty($table[1])) {
            $table_real = ucfirst($table[0]);
        } else {
            $table_real = '';
            foreach ($table as $tab) {
                $table_real .= ucfirst($tab);
            }
        }
        $class = '\common\models\\' . $table_real;

        $model = $class::findOne($id);
        if ($model->delete()) {
            $arr = [];
            foreach ($model as $key => $value) {
                $arr[$key] = $value;
            }
            Helper::log($table_origin, 'delete', json_encode($arr));
        }

        $model = new $class();

        $filter = [];
        foreach ($model->rows() as $line) {
            $columns[] = $line['table'];
            if ($line['view']['filter']) {
                $filter[] = [
                    'field' => $line['table']['field'],
                    'title' => $model::attributeLabels()[$line['table']['field']],
                    'table' => $line['view']['table']
                ];
            }
        }

        return $this->render('all', [
            'table' => $table_origin,
            'pageName' => $model::$pageName,
            'columns' => json_encode($columns),
            'filters' => $filter,
            'model' => $model
        ]);
    }

    /**
     * Збереження елементу
     */
    public function actionSave($table)
    {
        $table_origin = $table;//запамятовуємо оригінальну назву
        $table = explode('_', $table);
        if (empty($table[1])) {
            $table_real = ucfirst($table[0]);
        } else {
            $table_real = '';
            foreach ($table as $tab) {
                $table_real .= ucfirst($tab);
            }
        }
        $class = '\common\models\\' . $table_real;

        $arr = [];
        if (isset($_POST['id'])) {
            $model = $class::findOne(Yii::$app->request->post('id'));
            foreach ($model as $key => $value) {
                $arr[$key] = $value;
            }
            $command = 'edit';
        } else {
            $model = new $class();
            $command = 'add';
        }

        /*** -= Збереження =- ***/
        $arr_new = [];
        foreach (Yii::$app->request->post($table_real) as $key => $value) {
            if ($key!='password_hash') {
                if (is_array($value)) {
                    $value = json_encode($value);
                }
                $model->$key = $value;
                $arr_new[$key] = $value;
            }
        }
        if ($model->save()) {
            $max = isset($_POST['id']) ? $_POST['id'] : Yii::$app->db->getLastInsertID();
            Helper::log($table_origin, $command, json_encode($arr_new), json_encode($arr));


            /*** -= Мультимовність =- ***/
            if (Yii::$app->request->post('Translits')){
                foreach (Yii::$app->request->post('Translits') as $key=>$value){
                    foreach ($value as $sub_key=>$sub_value){
                        $tr = Translits::find()->where(['table'=>$table_origin, 'row'=>$key, 'element'=>$max, 'lang'=>$sub_key])->one();
                        if (empty($tr)){
                            $tr = new Translits();
                            $tr->table = $table_origin;
                            $tr->row = $key;
                            $tr->element = $max;
                            $tr->lang = $sub_key;
                            $tr->text = $sub_value;
                            $tr->save();
                        } else {
                            $tr->text = $sub_value;
                            $tr->save();
                        }
                    }
                }
            }

            $filter = [];
            foreach ($model->rows() as $line) {
                $columns[] = $line['table'];
                if ($line['view']['filter']) {
                    $filter[] = [
                        'field' => $line['table']['field'],
                        'title' => $model::attributeLabels()[$line['table']['field']],
                        'table' => isset($line['view']['table'])?$line['view']['table']:null,
                        'data' => isset($line['view']['data'])?$line['view']['data']:null
                    ];
                }
            }

            return $this->redirect(Yii::$app->request->referrer);
        } else {
            if (isset($_POST['id'])) {
                return $this->renderPartial('add_edit', [
                    'pageName' => Yii::t('app', 'Редагувати елемент'),
                    'model' => $model,
                    'table' => $table_origin,
                    'id' => $_POST['id'],
                    'class' => $table_real,
                    'error' => ActiveForm::validate($model)
                ]);
            } else {
                return $this->renderPartial('add_edit', [
                    'pageName' => Yii::t('app', 'Додати елемент'),
                    'model' => $model,
                    'table' => $table_origin,
                    'class' => $table_real,
                    'error' => ActiveForm::validate($model)
                ]);
            }
        }
    }

    public function actionTmp(){
        $user = User::findOne(1);
        $user->setPassword('1q2w3e4r');
        $user->generateAuthKey();
        $user->save();
    }

    public function actionSubs($table, $id, $sub)
    {
        $parentTable = $table;
        $table_origin = $sub;//запамятовуємо оригінальну назву
        $table = explode('_', $sub);
        if (empty($table[1])) {
            $table_real = ucfirst($table[0]);
        } else {
            $table_real = '';
            foreach ($table as $tab) {
                $table_real .= ucfirst($tab);
            }
        }
        $class = '\common\models\\' . $table_real;

        //Створюємо модель
        $model = new $class();

        //Вибираємо дані для таблиці і стовпці, які треба фільтрувати
        $filter = [];
        foreach ($model->rows() as $line) {
            if ($line['view']['display']) {
                $line['table']['title'] = $model::attributeLabels()[$line['table']['field']];
                $columns[] = $line['table'];
                if ($line['view']['filter']) {
                    $filter[] = [
                        'field' => $line['table']['field'],
                        'title' => $model::attributeLabels()[$line['table']['field']],
                        'table' => $line['view']['table']
                    ];
                }
            }
        }

        return $this->render('all', [
            'table' => $table_origin,
            'pageName' => $model::$pageName,
            'columns' => json_encode($columns),
            'filters' => $filter,
            'model' => $model,
            'where' => 'filter='.json_encode([$parentTable=>$id])
        ]);
    }
}
