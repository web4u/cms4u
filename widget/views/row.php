<?php
foreach ($items as $key => $value) {
    $href = \yii\helpers\Url::to('@web/' . $key);
    ?>
    <li class="m-menu__item  m-menu__item--submenu <?php if ($href == $active) echo 'm-menu__item--active'; ?>">
        <a href="<?php echo \yii\helpers\Url::to('@web/' . $key) ?>" class="m-menu__link">
            <i class="m-menu__link-icon fa <?php echo $value['icon'] ?>"></i>
            <span class="m-menu__link-text"><?= $value['name'] ?></span>
            <?php if (isset($value['pid'])){ ?><i class="m-menu__ver-arrow la la-angle-right"></i><?php } ?>
        </a>
        <?php
        if (isset($value['pid'])){
        ?>
        <div class="m-menu__submenu" style=""><span class="m-menu__arrow"></span>
            <ul class="m-menu__subnav">
                <?php
                if (isset($value['sub'])){
                foreach ($value['sub'] as $sub_key=>$sub_value){
                ?>
                <li class="m-menu__item m-menu__item--submenu" aria-haspopup="true" data-menu-submenu-toggle="hover">
                    <a href="/admin/<?=$sub_key?>" class="m-menu__link">
                        <i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i>
                        <span class="m-menu__link-text"><?=$sub_value?></span>
                    </a>
                </li>
                <?php }
                }

                if (isset($value['models'])){
                    foreach ($value['models'] as $models) {
                        $class = '\common\models\\' . $models['model'];
                        $model = new $class();
                        foreach ($model::find()->all() as $element) {
                            ?>
                            <li class="m-menu__item m-menu__item--submenu" aria-haspopup="true"
                                data-menu-submenu-toggle="hover">
                                <a href="/admin/<?=$key?>/<?= $element['id'] ?>/<?=$models['sub']?>" class="m-menu__link">
                                    <i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i>
                                    <span class="m-menu__link-text"><?= $element[$models['label']] ?></span>
                                </a>
                            </li>
                        <?php }
                    }
                    ?>
                    <li class="m-menu__item m-menu__item--submenu" aria-haspopup="true"
                        data-menu-submenu-toggle="hover">
                        <a href="/admin/<?=$key?>" class="m-menu__link">
                            <i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i>
                            <span class="m-menu__link-text">Перейти в раздел</span>
                        </a>
                    </li>
                    <?php
                }
                ?>
                <li class="m-menu__item m-menu__item--submenu" aria-haspopup="true" data-menu-submenu-toggle="hover">
                    <a href="/admin/texts/<?=$value['pid']?>" class="m-menu__link">
                        <i class="m-menu__link-bullet m-menu__link-bullet--dot"><span></span></i>
                        <span class="m-menu__link-text">Тексты</span>
                    </a>
                </li>
            </ul>
        </div>
        <?php } ?>
    </li>
<?php } ?>