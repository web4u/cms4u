<?php

namespace backend\widget;

use yii\base\Widget;
use yii\helpers\Html;

class MenuWidget extends Widget
{
    public $active;
    public $type;
    private $items;

    public function init()
    {
        parent::init();
        $this->items = [
            'elements' => [
                'sliders' => [
                    'name' => 'Слайдер',
                    'icon' => 'fa-file-image-o',
                ],
                'regions' => [
                    'name' => 'Регионы',
                    'icon' => 'fa-globe',
                ],
                'cities' => [
                    'name' => 'Города',
                    'icon' => 'fa-building',
                ],
                'specializations' => [
                    'name' => 'Специализации',
                    'icon' => 'fa-briefcase',
                ],
                'lawyers' => [
                    'name' => 'Юристы',
                    'icon' => 'fa-users',
                ],
                'orders' => [
                    'name' => 'Заказы',
                    'icon' => 'fa-file-o',
                ],
                'type_order' => [
                    'name' => 'Типы заказов',
                    'icon' => 'fa-cog',
                ],
                'docs' => [
                    'name' => 'Документы',
                    'icon' => 'fa-file-pdf',
                ]
            ],
            'static' => [
                '' => [
                    'name' => 'Dashboard',
                    'icon' => 'flaticon-line-graph'
                ],
                'pages' => [
                    'name' => 'Страницы',
                    'icon' => 'flaticon-file-1'
                ],
                'settings' => [
                    'name' => 'Настройки',
                    'icon' => 'flaticon-cogwheel'
                ]
            ]
        ];
    }

    public function run()
    {
        return $this->render('row', ['items' => $this->items[$this->type], 'active' => $this->active, 'type'=>$this->type]);
    }
}