<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "recipe_skills".
 *
 * @property integer $id
 * @property string $name
 */
class Settings extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'settings';
    }

    static $pageName='Настройки';

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['label', 'value', 'type'], 'required'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'label' => 'Название',
            'value' => 'Значение',
            'type' => 'Тип',
        ];
    }

    public function rows(){
        return [
            [
                'table'=>[
                    'field'=>'id',
                    'title'=>'ID',
                    'sortable'=>false,
                    'width'=>40,
                    'selector'=>['class'=>'m-checkbox--solid m-checkbox--brand'],
                    'textAlign'=>'center'
                ],
                'view'=>[
                    'filter'=>false,
                    'type'=>'none',
                    'display'=>true
                ]
            ],
        ];
    }
}
