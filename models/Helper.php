<?php
namespace backend\models;

use yii\base\Model;

class Helper extends Model{
    public function vdm($number, $after) {
        $cases = array (2, 0, 1, 1, 1, 2);
        return $number.' '.$after[ ($number%100>4 && $number%100<20)? 2: $cases[min($number%10, 5)] ];
    }

    public function log($table, $command, $text, $text_old=''){
        $log=new Logs();
        $log->data=date("Y-m-d");
        $log->time=date("H:i:s");
        $log->table=$table;
        $log->command=$command;
        $log->text=$text;
        $log->text_old=$text_old;
        $log->user=\Yii::$app->user->identity->username;
        $log->save();
    }

    public function getPicture($pic, $all=true, $default=false){
        $img=json_decode($pic);
        if (empty($img[0])){
            return $default==false?false:'/admin/source/default.jpeg';
        }
        return $all == true ? $img : $img[0];
    }
}