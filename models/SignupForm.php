<?php
namespace backend\models;

use yii\base\Model;
use backend\models\User;

/**
 * Signup form
 */
class SignupForm extends Model
{
    public $username;
    public $email;
    public $password;
    public $name;
    public $password_repeat;


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['username', 'trim'],
            ['name', 'trim'],
            ['username', 'required', 'message'=>\Yii::t('app', 'Поле не може бути пустим')],
            ['name', 'required', 'message'=>\Yii::t('app', 'Поле не може бути пустим')],
            ['username', 'unique', 'targetClass' => '\backend\models\User', 'message' => \Yii::t('app', 'Цей логін уже використовується')],
            ['username', 'string', 'min' => 2, 'max' => 255],

            ['email', 'trim'],
            ['email', 'required', 'message'=>\Yii::t('app', 'Поле не може бути пустим')],
            ['email', 'email'],
            ['email', 'string', 'max' => 255],
            ['email', 'unique', 'targetClass' => '\common\models\User', 'message' => \Yii::t('app', 'Цей email уже використовується')],

            ['password', 'required', 'message'=>\Yii::t('app', 'Поле не може бути пустим')],
            ['password_repeat', 'required', 'message'=>\Yii::t('app', 'Поле не може бути пустим')],
            ['password', 'string', 'min' => 6],
            ['password_repeat', 'string', 'min' => 6],
            ['password', 'compare', 'compareAttribute' => 'password_repeat', 'message' => \Yii::t('app', 'Паролі не співпадають')]
        ];
    }

    /**
     * Signs user up.
     *
     * @return User|null the saved model or null if saving fails
     */
    public function signup()
    {
        if (!$this->validate()) {
            return null;
        }

        $user = new \backend\models\User();
        $user->username = $this->username;
        $user->email = $this->email;
        $user->name=$this->name;
        $user->status=0;
        $user->setPassword($this->password);
        $user->generateAuthKey();

        return $user->save() ? $user : null;
    }
}
