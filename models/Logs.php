<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "logs".
 *
 * @property integer $id
 * @property string $data
 * @property string $time
 * @property string $command
 * @property string $table
 * @property string $text
 * @property string $text_old
 */
class Logs extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'logs';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['data', 'time'], 'safe'],
            [['text', 'text_old'], 'string'],
            [['command'], 'string', 'max' => 200],
            [['table'], 'string', 'max' => 100],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'data' => Yii::t('app', 'Data'),
            'time' => Yii::t('app', 'Time'),
            'command' => Yii::t('app', 'Command'),
            'table' => Yii::t('app', 'Table'),
            'text' => Yii::t('app', 'Text'),
            'text_old' => Yii::t('app', 'Text Old'),
        ];
    }
}
