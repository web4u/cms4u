<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "recipe_skills".
 *
 * @property integer $id
 * @property string $name
 */
class Elements extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'elements';
    }

    static $pageName='Элементы';

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['page_id', 'type', 'value'], 'safe'],
        ];
    }


    public function rows(){
        return [];
    }
}
